#include "polydata.h"

polydata::polydata(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

void polydata::on_pushButton_clicked()
{
	
	if (ui.comboBox->currentIndex() == 0) 
	{
		troj = new Data<Trojuholnik>(ui.doubleSpinBox->value(), ui.doubleSpinBox_2->value(), 
			ui.doubleSpinBox_3->value(), ui.doubleSpinBox_4->value(), ui.doubleSpinBox_5->value(), ui.doubleSpinBox_6->value(),
			ui.doubleSpinBox_7->value(), ui.doubleSpinBox_8->value(), ui.spinBox->value(), ui.spinBox_2->value());
		
		ui.tableWidget->setRowCount(ui.spinBox_2->value() + 1);
		ui.tableWidget->setColumnCount(ui.spinBox->value() + 1);
		
		for (int i = 0; i <= ui.spinBox_2->value(); i++) 
		{
			for (int j = 0; j <= ui.spinBox->value(); j++) 
			{
				QTableWidgetItem *newItem = new QTableWidgetItem(QString("[%1,%2,%3]").arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajX()).arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajY()).arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajZ()));
				ui.tableWidget->setItem(i, j, newItem);
			}
		}
	}

	else 
	{
		obd = new Data<Obdlznik>(ui.doubleSpinBox->value(), ui.doubleSpinBox_2->value(),
			ui.doubleSpinBox_3->value(), ui.doubleSpinBox_4->value(), ui.doubleSpinBox_5->value(), ui.doubleSpinBox_6->value(),
			ui.doubleSpinBox_7->value(), ui.doubleSpinBox_8->value(), ui.spinBox->value(), ui.spinBox_2->value());
		
		ui.tableWidget->setRowCount(ui.spinBox_2->value() + 1);
		ui.tableWidget->setColumnCount(ui.spinBox->value() + 1);

		for (int i = 0; i <= ui.spinBox_2->value(); i++) 
		{
			for (int j = 0; j <= ui.spinBox->value(); j++) 
			{
				QTableWidgetItem *newItem = new QTableWidgetItem(QString("[%1,%2,%3]").arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajX()).arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajY()).arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajZ()));
				ui.tableWidget->setItem(i, j, newItem);
			}
		}
	}
}

void polydata::on_actionsave_triggered() 
{
	QString fileName = QFileDialog::getSaveFileName(this, "Save Image", "", "Text Files(*.txt)");
	QFile  file(fileName);
	if (file.open(QIODevice::WriteOnly | QIODevice::Text)) 
	{
		QTextStream stream(&file);
		
		if (ui.comboBox->currentIndex() == 0) 
		{
			stream<<"POINTS\n";
			for (int i = 0; i < troj->dajBodysize(); i++)
				stream<<troj->dajBody()[i].dajX()<<" "<<troj->dajBody()[i].dajY()<<" "<<troj->dajBody()[i].dajZ()<<"\n";

			int k = 0;
			int pom = ui.spinBox->value();
			for (int i = 0; i<ui.spinBox_2->value()*ui.spinBox->value() + ui.spinBox_2->value() - 1; i++) 
			{
				if (i != pom)
				{
					troj->pushBack(Trojuholnik(i, i + 1, i + ui.spinBox->value() + 1));
					troj->pushBack(Trojuholnik(i + 1, i + ui.spinBox->value() + 2, i + ui.spinBox->value() + 1));
					k++;
				}
				else
					pom += ui.spinBox->value() + 1;
			}

			stream<<"POLYDATA\n";
			for (int i = 0; i <troj->dajVektorSize(); i++) 
				stream<<troj->dajVektor()[i].dajV1()<<" "<<troj->dajVektor()[i].dajV2()<<" "<<
					troj->dajVektor()[i].dajV3()<<"\n";
		}

		else 
		{
			stream<<"POINTS:\n";
			for (int i = 0; i < obd->dajVektorSize(); i++) 
				stream<<obd->dajBody()[i].dajX()<<" "<<obd->dajBody()[i].dajY()<<" "<<obd->dajBody()[i].dajZ()<<"\n";


			int pom = ui.spinBox->value(), k = 0;
			for (int i = 0; i<ui.spinBox_2->value()*ui.spinBox->value() + ui.spinBox_2->value() - 1; i++) 
			{
				if (i != pom) 
				{
					obd->pushBack(Obdlznik(i, i + 1, i + ui.spinBox->value() + 1, i + ui.spinBox->value() + 2));
					k++;
				}
				else 
					pom += ui.spinBox->value() + 1;
			
			}

			stream<<"POLYDATA\n";
			for (int i = 0; i < obd->dajVektorSize(); i++) 
				stream<<obd->dajVektor()[i].dajV1()<<" "<<obd->dajVektor()[i].dajV2()<<" "<<obd->dajVektor()[i].dajV3()<<obd->dajVektor()[i].dajV4()<<"\n";
		
		}
		
		file.close();
	}
}

void polydata::on_actionopen_triggered() 
{
	QString fileName = QFileDialog::getOpenFileName(this, "Open", "", "Text Files (*.txt)");
	QFile file(fileName);
	QStringList list;
	QString line;

	std::vector<Bod> B;
	std::vector<Trojuholnik> T;
	std::vector<Obdlznik> O;

	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
	{
		QTextStream stream(&file);
		line = stream.readLine();
		while (line != "POLYDATA") {
			line = stream.readLine();
			if (line == "POLYDATA") {
				break;
			}
			list = line.split(' ');
			qDebug() << list.at(0)<<" "<<list.at(1)<<" "<<list.at(2)<< " ";
			B.push_back(Bod(list.at(0).toDouble(), list.at(1).toDouble(), list.at(2).toDouble()));
		}

		qDebug()<<"\n";
		line = stream.readLine();
		list = line.split(' ');
		if (list.size() == 3) 
		{
			troj = new Data<Trojuholnik>(B, T);
			qDebug()<<list.at(0)<<" "<<list.at(1)<<" "<<list.at(2);
			T.push_back(Trojuholnik(list.at(0).toInt(), list.at(1).toInt(), list.at(2).toInt()));
			while (!stream.atEnd()) 
			{
				line = stream.readLine();
				list = line.split(' ');
				qDebug() << list.at(0)<<" "<<list.at(1)<<" "<<list.at(2);
				T.push_back(Trojuholnik(list.at(0).toInt(), list.at(1).toInt(), list.at(2).toInt()));
			}

			ui.tableWidget->setRowCount(ui.spinBox_2->value() + 1);
			ui.tableWidget->setColumnCount(ui.spinBox->value() + 1);

			for (int i = 0; i <= ui.spinBox_2->value(); i++) 
			{
				for (int j = 0; j <= ui.spinBox->value(); j++) 
				{
					QTableWidgetItem *newItem = new QTableWidgetItem(QString("[%1,%2,%3]").arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajX()).arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajY()).arg(troj->dajBody()[i*(ui.spinBox->value() + 1) + j].dajZ()));
					ui.tableWidget->setItem(i, j, newItem);
				}
			}
		}

		else 
		{
			obd = new Data<Obdlznik>(B, O);
			qDebug() << list.at(0)<<" "<<list.at(1)<<" "<<list.at(2)<<" "<<list.at(3).toInt();
			O.push_back(Obdlznik(list.at(0).toInt(), list.at(1).toInt(), list.at(2).toInt(), list.at(3).toInt()));
			while (!stream.atEnd()) 
			{
				line = stream.readLine();
				list = line.split(' ');
				qDebug()<<list.at(0)<<" "<<list.at(1)<<" "<<list.at(2)<<" "<<list.at(3)<<" ";
				O.push_back(Obdlznik(list.at(0).toInt(), list.at(1).toInt(), list.at(2).toInt(), list.at(3).toInt()));
			}

			ui.tableWidget->setRowCount(ui.spinBox_2->value() + 1);
			ui.tableWidget->setColumnCount(ui.spinBox->value() + 1);
			for (int i = 0; i <= ui.spinBox_2->value(); i++)
			{
				for (int j = 0; j <= ui.spinBox->value(); j++)
				{
					QTableWidgetItem *newItem = new QTableWidgetItem(QString("[%1,%2,%3]").arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajX()).arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajY()).arg(obd->dajBody()[i*(ui.spinBox->value() + 1) + j].dajZ()));
					ui.tableWidget->setItem(i, j, newItem);
				}
			}
		}
	}
}