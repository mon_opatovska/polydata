#ifndef POLYDATA_H
#define POLYDATA_H

#include <QtWidgets/QMainWindow>
#include "ui_polydata.h"
#include <QDebug>
#include <QTableWidget> 
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <vector>

class Bod
{
private:
	double x, y, z;

public:
	Bod() {}
	Bod(double x, double y, double z) { this->x = x; this->y = y; this->z = z; }


	double dajX() { return x; }
	double dajY() { return y; }
	double dajZ() { return z; }
};

class Trojuholnik
{
private:
	int v1, v2, v3;

public:
	Trojuholnik(){}
	Trojuholnik (int v1, int v2, int v3) { this->v1 = v1; this->v2 = v2; this->v3 = v3; }

	int dajV1() { return v1; }
	int dajV2() { return v2; }
	int dajV3() { return v3; }
};

class Obdlznik
{
private:
	int v1, v2, v3, v4;

public:
	Obdlznik (){}
	Obdlznik(int v1, int v2, int v3, int v4) { this->v1 = v1; this->v2 = v2; this->v3 = v3; this->v4 = v4; }

	int dajV1() { return v1; }
	int dajV2() { return v2; }
	int dajV3() { return v3; }
	int dajV4() { return v4; }
};

template <class T>
class Data
{
	
private:
	std::vector<Bod> body;
	std::vector<T> u;
	double dx, dy, xmin, xmax, ymin, ymax, posunx, posuny;

public:
	Data() {}
	Data(std::vector<Bod> B, std::vector<T> U);
	Data(double a, double b, double c, double d, double k, double l, double m, double n, int dX, int dY);
	std::vector<Bod> dajBody() { return body; }
	std::vector<T> dajVektor() { return u; }

	void pushBack(T o) { u.push_back(o);}
	int dajBodysize() { return body.size(); }
	int dajVektorSize() { return u.size(); }
};

template <class T>
Data<T>::Data(std::vector<Bod> B, std::vector<T> U)
{
	body = B;
	u = U;
	
	int poc = 1;
	double x, xx;
	x = B[0].dajX();
	do 
	{
		xx = B[poc].dajX();
		poc++;
	} while (xx != x);

	dx = poc - 2;
	dy = B.size() / (dx + 1) - 1;
}

template<class T>
Data<T>::Data(double xmin, double xmax, double ymin, double ymax, double a, double b, double c, double d, int dx, int dy)
{
	this->xmin = xmin;
	this->xmax = xmax;
	this->ymin = ymin;
	this->ymax = ymax;
	this->dx = dx;
	this->dy = dy;
	
	posunx = (xmax - xmin) / dx;
	posuny = (ymax - ymin) / dy;
	
	for (int i = 0; i < dy + 1; i++) 
	{
		for (int j = 0; j < dx + 1; j++)
			body.push_back(Bod(xmin + j*posunx, ymin + i*posuny, a*pow((xmin + j*posunx), b) + c*pow((ymin + i*posuny), d)));
	}
}

class polydata : public QMainWindow
{
	Q_OBJECT

public:
	polydata(QWidget *parent = 0);

private:
	Ui::polydataClass ui;
	Data<Trojuholnik> *troj;
	Data<Obdlznik> *obd;
	

private slots:
	void on_pushButton_clicked();
	void on_actionsave_triggered();
	void on_actionopen_triggered();
};

#endif // POLYDATA_H
