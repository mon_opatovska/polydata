/********************************************************************************
** Form generated from reading UI file 'polydata.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_POLYDATA_H
#define UI_POLYDATA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_polydataClass
{
public:
    QAction *actionopen;
    QAction *actionsave;
    QWidget *centralWidget;
    QTableWidget *tableWidget;
    QLabel *label;
    QSpinBox *spinBox;
    QLabel *label_2;
    QSpinBox *spinBox_2;
    QLabel *label_3;
    QDoubleSpinBox *doubleSpinBox;
    QLabel *label_4;
    QDoubleSpinBox *doubleSpinBox_2;
    QLabel *label_5;
    QDoubleSpinBox *doubleSpinBox_3;
    QLabel *label_6;
    QDoubleSpinBox *doubleSpinBox_4;
    QLabel *label_7;
    QLabel *label_8;
    QDoubleSpinBox *doubleSpinBox_5;
    QLabel *label_9;
    QDoubleSpinBox *doubleSpinBox_6;
    QLabel *label_10;
    QDoubleSpinBox *doubleSpinBox_7;
    QLabel *label_11;
    QDoubleSpinBox *doubleSpinBox_8;
    QLabel *label_12;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QMenu *menumenu;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *polydataClass)
    {
        if (polydataClass->objectName().isEmpty())
            polydataClass->setObjectName(QStringLiteral("polydataClass"));
        polydataClass->resize(881, 553);
        actionopen = new QAction(polydataClass);
        actionopen->setObjectName(QStringLiteral("actionopen"));
        actionsave = new QAction(polydataClass);
        actionsave->setObjectName(QStringLiteral("actionsave"));
        centralWidget = new QWidget(polydataClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tableWidget = new QTableWidget(centralWidget);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(420, 10, 441, 491));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 20, 47, 21));
        QFont font;
        font.setPointSize(10);
        label->setFont(font);
        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setGeometry(QRect(40, 20, 42, 22));
        spinBox->setMinimum(1);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 60, 47, 21));
        label_2->setFont(font);
        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setGeometry(QRect(40, 60, 42, 22));
        spinBox_2->setMinimum(1);
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(110, 20, 47, 21));
        label_3->setFont(font);
        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setGeometry(QRect(150, 20, 62, 22));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(110, 60, 47, 21));
        label_4->setFont(font);
        doubleSpinBox_2 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_2->setObjectName(QStringLiteral("doubleSpinBox_2"));
        doubleSpinBox_2->setGeometry(QRect(150, 60, 62, 22));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(240, 20, 47, 21));
        label_5->setFont(font);
        doubleSpinBox_3 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_3->setObjectName(QStringLiteral("doubleSpinBox_3"));
        doubleSpinBox_3->setGeometry(QRect(290, 20, 62, 22));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(240, 60, 47, 21));
        label_6->setFont(font);
        doubleSpinBox_4 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_4->setObjectName(QStringLiteral("doubleSpinBox_4"));
        doubleSpinBox_4->setGeometry(QRect(290, 60, 62, 22));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(20, 120, 101, 16));
        label_7->setFont(font);
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(20, 150, 47, 21));
        label_8->setFont(font);
        doubleSpinBox_5 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_5->setObjectName(QStringLiteral("doubleSpinBox_5"));
        doubleSpinBox_5->setGeometry(QRect(30, 150, 62, 22));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(110, 150, 47, 21));
        label_9->setFont(font);
        doubleSpinBox_6 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_6->setObjectName(QStringLiteral("doubleSpinBox_6"));
        doubleSpinBox_6->setGeometry(QRect(120, 150, 62, 22));
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(200, 150, 47, 21));
        label_10->setFont(font);
        doubleSpinBox_7 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_7->setObjectName(QStringLiteral("doubleSpinBox_7"));
        doubleSpinBox_7->setGeometry(QRect(210, 150, 62, 22));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(290, 150, 47, 21));
        label_11->setFont(font);
        doubleSpinBox_8 = new QDoubleSpinBox(centralWidget);
        doubleSpinBox_8->setObjectName(QStringLiteral("doubleSpinBox_8"));
        doubleSpinBox_8->setGeometry(QRect(300, 150, 62, 22));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(20, 210, 47, 16));
        label_12->setFont(font);
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(60, 210, 91, 22));
        comboBox->setFont(font);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(150, 460, 81, 31));
        pushButton->setFont(font);
        polydataClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(polydataClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 881, 21));
        menumenu = new QMenu(menuBar);
        menumenu->setObjectName(QStringLiteral("menumenu"));
        polydataClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(polydataClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        polydataClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(polydataClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        polydataClass->setStatusBar(statusBar);

        menuBar->addAction(menumenu->menuAction());
        menumenu->addAction(actionopen);
        menumenu->addAction(actionsave);

        retranslateUi(polydataClass);

        QMetaObject::connectSlotsByName(polydataClass);
    } // setupUi

    void retranslateUi(QMainWindow *polydataClass)
    {
        polydataClass->setWindowTitle(QApplication::translate("polydataClass", "polydata", Q_NULLPTR));
        actionopen->setText(QApplication::translate("polydataClass", "otvori\305\245", Q_NULLPTR));
        actionsave->setText(QApplication::translate("polydataClass", "ulo\305\276i\305\245", Q_NULLPTR));
        label->setText(QApplication::translate("polydataClass", "dx", Q_NULLPTR));
        label_2->setText(QApplication::translate("polydataClass", "dy", Q_NULLPTR));
        label_3->setText(QApplication::translate("polydataClass", "Xmin", Q_NULLPTR));
        label_4->setText(QApplication::translate("polydataClass", "Ymin", Q_NULLPTR));
        label_5->setText(QApplication::translate("polydataClass", "Xmax", Q_NULLPTR));
        label_6->setText(QApplication::translate("polydataClass", "Ymax", Q_NULLPTR));
        label_7->setText(QApplication::translate("polydataClass", "z= ax^b + cy^d", Q_NULLPTR));
        label_8->setText(QApplication::translate("polydataClass", "a", Q_NULLPTR));
        label_9->setText(QApplication::translate("polydataClass", "b", Q_NULLPTR));
        label_10->setText(QApplication::translate("polydataClass", "c", Q_NULLPTR));
        label_11->setText(QApplication::translate("polydataClass", "d", Q_NULLPTR));
        label_12->setText(QApplication::translate("polydataClass", "sie\305\245", Q_NULLPTR));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("polydataClass", "trojuholn\303\255k", Q_NULLPTR)
         << QApplication::translate("polydataClass", "obd\304\272\305\276nik", Q_NULLPTR)
        );
        pushButton->setText(QApplication::translate("polydataClass", "generuj", Q_NULLPTR));
        menumenu->setTitle(QApplication::translate("polydataClass", "menu", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class polydataClass: public Ui_polydataClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_POLYDATA_H
